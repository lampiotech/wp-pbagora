<?php
use Ampersand\Route;

Ampersand\Render::getTwig()->addGlobal('template_path', "/wp-content/themes/wp-pbagora/");
Route::get('/', function() {
  $this->render('index');
});
